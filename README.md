# Uber_Nodejs

Implement UBER like service for freight trucks, in REST style, using MongoDB as database.
This service should help regular people to deliver their stuff and help drivers to find loads
and earn some money. Application contains 2 roles, driver and shipper.

- register and login with JWT authentication;
- ability to get and delete user's profile;
- a driver can add a new truck, update/delete/assign it;
- a shipper can add a new load, update/delete it and look for a driver;

Database: MongoDB;
Technologies: Mongoose, MongoDB Atlas, Node JS, Express, JWT, Swagger UI, morgan, bcrypt, eslint, postman

###

To run the project: npm run start
