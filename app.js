const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient;
const jwt = require('jsonwebtoken');
const joi = require('joi');
const Load = require('./models/load');
const User = require('./models/user');
const Truck = require('./models/truck');

mongoose.connect('mongodb+srv://kristik991:verySafePass@cluster0.0i8dl.mongodb.net/UberForTrucks?retryWrites=true&w=majority',
    {useNewUrlParser: true, useUnifiedTopology: true}).then(() => {
  console.log('MONGO CONNECTION OPEN!!!');
})
    .catch((err) => {
      console.log('OH NO MONGO CONNECTION ERROR!!!!');
      console.log(err);
    });

require('dotenv').config();

app.use(cors());
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/api/trucks/*', (req, res, next) => {
  const url = req.originalUrl;
  const word = url.split('/');
  const newW = word[word.length - 1];
  if (!newW) {
    return res.status(200).json({message: 'ID parameter required'});
  } else {
    next();
  }
});

app.use('/api/loads/*', (req, res, next) => {
  const url = req.originalUrl;
  const word = url.split('/');
  const newW = word[word.length - 1];
  if (!newW) {
    return res.status(200).json({message: 'ID parameter required'});
  } else {
    next();
  }
});

app.use('/api/loads/*/post', (req, res, next) => {
  const url = req.originalUrl;
  const word = url.split('/');
  const newW = word[word.length - 2];
  if (!newW) {
    return res.status(200).json({message: 'ID parameter required'});
  } else {
    next();
  }
});

/**
 * Defines a type of a truck.
 * @param {int} payload a payload.
 * @param {int} width a width.
 * @param {int} length a length.
 * @param {int} height a height.
 * @return {array} possible truck types.
 */
function defineTruckType(payload, width, length, height) {
  if (payload < 1700 && width < 300 && length < 250 && height < 170) {
    return ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
  } else if (payload < 2500 && width < 500 && length < 250 && height < 170) {
    return ['SMALL STRAIGHT', 'LARGE STRAIGHT'];
  } else if (payload < 4000 && width < 700 && length < 350 && height < 200) {
    return ['LARGE STRAIGHT'];
  }
}

const checkToken = (req, res, next) => {
  const token = req.headers.authorization;
  let key = token;
  try {
    if (!token) {
      return res.status(400).json(
          {message: 'A token is required for authentication'});
    }
    if (token.split(' ')[0] === 'JWT' || token.split(' ')[0] === 'Bearer') {
      key = token.split(' ')[1];
    }
    const decoded = jwt.verify(key, process.env.TOKEN_KEY);
    req.user = decoded;
  } catch (err) {
    return res.status(400).json({message: 'Invalid token'});
  }
  return next();
};
// PROFILE
app.get('/api/users/me', checkToken, async function getProfileInfo(req, res) {
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        const userFromDB = await User.findOne({email: user.email});
        if (!userFromDB) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        db.close();
        return res.status(200).json({
          user: {
            _id: userFromDB._id,
            role: userFromDB.role,
            email: userFromDB.email,
            created_date: userFromDB.created_date,
          },
        });
      }
    }
  });
});

app.delete('/api/users/me', checkToken, async function deleteProfile(req, res) {
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        try {
          const userFromDB = await User.findOne({email: user.email});
          if (!userFromDB) {
            db.close();
            return res.status(400).json({message: 'Username does not exist'});
          }
          if (userFromDB.role === 'DRIVER') {
            const isAssignedTruck = await Truck.findOne(
                {assigned_to: userFromDB._id,
                  status: 'OL'});
            if (!isAssignedTruck) {
              await Truck.deleteMany({created_by: userFromDB._id});
            } else {
              db.close();
              return res.status(400).json({message: 'You have assigned truck'});
            }
          } else if (userFromDB.role === 'SHIPPER') {
            const newLoads = await Load.findOne(
                {assigned_to: {$ne: null}, status: {$ne: 'SHIPPED'}});
            if (!newLoads) {
              await Load.deleteMany({created_by: userFromDB._id});
            } else {
              db.close();
              return res.status(400).json({message: 'You have active load'});
            }
          }
          await User.findOneAndDelete({email: user.email});
          db.close();
          return res.status(200).json({message:
            'Profile deleted successfully'});
        } catch (e) {
          db.close();
          return res.status(400).json({message: 'Invalid request'});
        }
      }
    }
  });
});

app.patch('/api/users/me/password', checkToken,
    async function changeProfilePassword(req, res) {
      const {body} = req;
      const changeProfilePaswordSchema = joi.object({
        oldPassword: joi.string().required(),
        newPassword: joi.string().required(),
      });
      const {error} = changeProfilePaswordSchema.validate(body);
      const valid = error == null;
      if (!valid) {
        res.status(400).json({
          message: 'oldPassword and newPassword are required',
        });
      } else {
        MongoClient.connect(process.env.DB_URI, async function(err, db) {
          if (err) {
            db.close();
            return res.status(500).json({message: 'Server error'});
          } else {
            const user = req.user;
            if (!user) {
              return res.status(400).json({message: 'Cannot find your token'});
            } else {
              const userFromDB = await User.findOne({email: user.email});
              if (!userFromDB) {
                db.close();
                return res.status(400).json({
                  message: 'Username does not exist'});
              }
              const result = bcrypt.compareSync(
                  body.oldPassword, userFromDB.hash,
              );
              if (!result) {
                db.close();
                return res.status(400).json({
                  message: 'OldPassword is incorrect'});
              } else {
                const newHash = bcrypt.hashSync(body.newPassword, 10);
                await User.findOneAndUpdate(
                    {email: user.email},
                    {hash: newHash},
                    {new: true},
                );
                db.close();
                return res.status(200).json({
                  message: 'Password changed successfully',
                });
              }
            }
          }
        });
      }
    });
// TRUCKS
app.get('/api/trucks', checkToken, async function getUserTrucks(req, res) {
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        const userFromDB = await User.findOne({email: user.email});
        if (!userFromDB) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        if (userFromDB.role !== 'DRIVER') {
          db.close();
          return res.status(400).json(
              {message: 'Only a DRIVER is authorized to access this url'});
        }
        const trucksFromDB = await Truck.find({created_by: userFromDB._id});
        db.close();
        return res.status(200).json({
          trucks: trucksFromDB,
        });
      }
    }
  });
});

app.post('/api/trucks', checkToken, async function addUserTruck(req, res) {
  const {body} = req;

  const userTruckSchema = joi.object({
    type: joi.string().valid(
        'SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required(),
  });

  const {error} = userTruckSchema.validate(body);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid request',
    });
  } else {
    MongoClient.connect(process.env.DB_URI, async function(err, db) {
      if (err) {
        db.close();
        return res.status(500).json({message: 'Server error'});
      } else {
        const user = req.user;
        if (!user) {
          return res.status(400).json({message: 'Cannot find your token'});
        } else {
          const userFromDB = await User.findOne({email: user.email});
          if (!userFromDB) {
            db.close();
            return res.status(400).json({message: 'Username does not exist'});
          }
          if (userFromDB.role !== 'DRIVER') {
            db.close();
            return res.status(400).json(
                {message: 'Only a DRIVER is authorized to access this url'});
          }
          const date = new Date();
          const newTruck = new Truck({
            created_by: userFromDB._id,
            assigned_to: null,
            type: body.type,
            status: 'IS',
            created_date: date.toString(),
          });
          await newTruck.save();
          db.close();
          return res.status(200).json(
              {message: 'Truck created successfully'});
        }
      }
    });
  }
});

app.get('/api/trucks/:id', checkToken,
    async function getUserTruckById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              const truckFromDB = await Truck.findOne(
                  {created_by: userFromDB._id, _id: id});
              if (!userFromDB || !truckFromDB) {
                db.close();
                return res.status(400).json(
                    {message: `No user or truck found`});
              }
              if (userFromDB.role !== 'DRIVER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a DRIVER is authorized to access this url'});
              }
              return res.status(200).json({
                truck: truckFromDB});
            } catch (e) {
              db.close();
              return res.status(400).json({message: `Invalid request`});
            }
          }
        }
      });
    });

app.put('/api/trucks/:id', checkToken,
    async function updateUserTruckById(req, res) {
      const {body, params} = req;
      const updateTruckSchema = joi.object({type: joi.string().valid(
          'SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required()});
      const {error} = updateTruckSchema.validate(body);
      const valid = error == null;
      if (!valid) {
        res.status(400).json({message: 'Type is incorrect',
        });
      } else {
        MongoClient.connect(process.env.DB_URI, async function(err, db) {
          if (err) {
            db.close();
            return res.status(500).json({message: 'Server error'});
          } else {
            const user = req.user;
            if (!user) {
              return res.status(400).json({message: 'Cannot find your token'});
            } else {
              try {
                const userFromDB = await User.findOne({email: user.email});
                if (userFromDB.role !== 'DRIVER') {
                  db.close();
                  return res.status(400).json(
                      {message:
                        'Only a DRIVER is authorized to access this url'});
                }
                const assignedTruck = await Truck.findOne(
                    {created_by: userFromDB._id,
                      _id: params.id, assigned_to: userFromDB._id});
                if (assignedTruck) {
                  return res.status(400).json(
                      {message: 'You cannot update a truck that is assigned'});
                }
                await Truck.findOneAndUpdate({
                  created_by: userFromDB._id, _id: params.id},
                {type: body.type});
                db.close();
                return res.status(200).json(
                    {message: 'Truck details changed successfully'});
              } catch (e) {
                if (e) {
                  db.close();
                  return res.status(400).json({
                    message: `Invalid request`,
                  });
                }
              }
            }
          }
        });
      }
    });

app.delete('/api/trucks/:id', checkToken,
    async function deleteUserTruckById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json(
                {message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'DRIVER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a DRIVER is authorized to access this url'});
              }
              const assignedTruck = await Truck.findOne(
                  {created_by: userFromDB._id,
                    _id: id, assigned_to: userFromDB._id});
              if (assignedTruck) {
                return res.status(400).json(
                    {message: 'You cannot delete a truck that is assigned'});
              }
              await Truck.findOneAndDelete(
                  {created_by: userFromDB._id, _id: id});
              db.close();
              return res.status(200).json(
                  {message: 'Truck deleted successfully'});
            } catch (e) {
              if (e) {
                db.close();
                return res.status(400).json({
                  message: `Invalid request`,
                });
              }
            }
          }
        }
      });
    });

app.post('/api/trucks/:id/assign', checkToken,
    async function assignUserTruckById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json(
                {message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'DRIVER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a DRIVER is authorized to access this url'});
              }
              await Truck.updateMany(
                  {created_by: userFromDB._id, assigned_to: userFromDB._id,
                    status: 'IS'}, {assigned_to: null});
              const myAssignedTruck = await Truck.findOneAndUpdate(
                  {created_by: userFromDB._id, _id: id, status: 'IS'},
                  {assigned_to: userFromDB._id});
              if (!myAssignedTruck) {
                db.close();
                return res.status(400).json(
                    {message: 'You do not have a truck with this ID'});
              }
              db.close();
              return res.status(200).json(
                  {message: 'Truck assigned successfully'});
            } catch (e) {
              if (e) {
                db.close();
                return res.status(400).json({message: 'Invalid request'});
              }
            }
          }
        }
      });
    });
// LOADS
app.get('/api/loads', checkToken, async function getUserLoads(req, res) {
  const {query} = req;
  const getUserLoadsSchema = joi.object({
    status: joi.string(),
    limit: joi.number().default(10).max(50),
    offset: joi.number().default(0),
  });
  const {error} = getUserLoadsSchema.validate(query);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid status, offset or limit',
    });
  } else {
    MongoClient.connect(process.env.DB_URI, async function(err, db) {
      if (err) {
        db.close();
        return res.status(500).json({message: 'Server error'});
      } else {
        const user = req.user;
        if (!user) {
          return res.status(400).json({message: 'Cannot find your token'});
        } else {
          try {
            const userFromDB = await User.findOne({email: user.email});
            if (userFromDB.role === 'DRIVER') {
              if (!query.status && !query.limit && !query.offset) {
                const driversLoads = await Load.find(
                    {assigned_to: userFromDB._id});
                db.close();
                return res.status(200).json(
                    {loads: driversLoads});
              }
              const driversLoads = await Load.find(
                  {assigned_to: userFromDB._id})
                  .skip(+query.offset)
                  .limit(+query.limit)
                  .where('status').equals(query.status);
              db.close();
              return res.status(200).json(
                  {loads: driversLoads});
            } else if (userFromDB.role === 'SHIPPER') {
              if (!query.status && !query.limit && !query.offset) {
                const shippersLoads = await Load.find(
                    {created_by: userFromDB._id});
                db.close();
                return res.status(200).json(
                    {loads: shippersLoads});
              }
              const shippersLoads = await Load.find(
                  {created_by: userFromDB._id})
                  .skip(+query.offset)
                  .limit(+query.limit)
                  .where('status').equals(query.status);
              db.close();
              return res.status(200).json(
                  {loads: shippersLoads});
            }
          } catch (e) {
            db.close();
            return res.status(400).json(
                {message: 'Invalid request'});
          }
        }
      }
    });
  }
});

app.post('/api/loads/:id/post', checkToken,
    async function postUserLoadById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'SHIPPER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a SHIPPER is authorized to access this url'});
              }
              const loadFromDB = await Load.findOne(
                  {created_by: userFromDB._id, _id: id});
              if (loadFromDB.status !== 'NEW') {
                db.close();
                return res.status(400).json(
                    {message: 'Load is already posted'});
              }
              const datePosted = new Date();
              const loadPosted = await Load.findOneAndUpdate(
                  {created_by: userFromDB._id, _id: id},
                  {status: 'POSTED'}, {new: true});
              loadPosted.logs.push({message: `Load posted`,
                time: datePosted.toString()});
              loadPosted.save();
              const payload = loadFromDB.payload;
              const width = loadFromDB.dimensions.width;
              const length = loadFromDB.dimensions.length;
              const height = loadFromDB.dimensions.height;
              const truckType = defineTruckType(payload, width, length, height);
              const isTruckFound = await Truck.findOneAndUpdate(
                  {status: 'IS', assigned_to: {$ne: null},
                    type: {$in: truckType}}, {status: 'OL'}, {new: true});
              if (!isTruckFound) {
                const dateNotFound = new Date();
                const noTruckLoad = await Load.findOneAndUpdate(
                    {created_by: userFromDB._id, _id: id},
                    {status: 'NEW'}, {new: true});
                noTruckLoad.logs.push(
                    {message: 'No truck found, status is NEW',
                      time: dateNotFound.toString()});
                noTruckLoad.save();
                db.close();
                return res.status(400).json({message: 'Truck was not found'});
              }
              const foundTruckLoad = await Load.findOneAndUpdate(
                  {created_by: userFromDB._id, _id: id},
                  {status: 'ASSIGNED', state: 'En route to Pick Up',
                    assigned_to: isTruckFound.created_by}, {new: true});
              const dateFound = new Date();
              foundTruckLoad.logs.push({message:
                `Load assigned to driver with id ${userFromDB._id}`,
              time: dateFound.toString()});
              foundTruckLoad.save();
              db.close();
              return res.status(200).json(
                  {message: 'Load posted successfully',
                    driver_found: true});
            } catch (e) {
              db.close();
              return res.status(400).json({message: 'Invalid request'});
            }
          }
        }
      });
    });

app.get('/api/loads/:id/shipping_info', checkToken,
    async function getUserLoadShippingDetailsById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'SHIPPER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a SHIPPER is authorized to access this url'});
              }
              const loadFromDB = await Load.findOne(
                  {created_by: userFromDB._id, _id: id});
              if (loadFromDB.status !== 'ASSIGNED') {
                db.close();
                return res.status(400).json(
                    {message: 'Load is not active or does not exist'});
              }
              const truckFromDB = await Truck.findOne(
                  {assigned_to: loadFromDB.assigned_to, status: 'OL'});
              db.close();
              return res.status(200).json(
                  {load: loadFromDB, truck: truckFromDB});
            } catch (e) {
              db.close();
              return res.status(400).json({message: 'Invalid request'});
            }
          }
        }
      });
    });

app.post('/api/loads', checkToken, async function addUserLoad(req, res) {
  const {body} = req;

  const userLoadSchema = joi.object({
    name: joi.string().required(),
    payload: joi.number().required(),
    pickup_address: joi.string().required(),
    delivery_address: joi.string().required(),
    dimensions: joi.object().keys({
      width: joi.number().required(),
      length: joi.number().required(),
      height: joi.number().required(),
    }),
  });

  const {error} = userLoadSchema.validate(body);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid request',
    });
  } else {
    MongoClient.connect(process.env.DB_URI, async function(err, db) {
      if (err) {
        db.close();
        return res.status(500).json({message: 'Server error'});
      } else {
        const user = req.user;
        if (!user) {
          return res.status(400).json({message: 'Cannot find your token'});
        } else {
          try {
            const userFromDB = await User.findOne({email: user.email});
            if (userFromDB.role !== 'SHIPPER') {
              db.close();
              return res.status(400).json(
                  {message: 'Only a SHIPPER is authorized to access this url'});
            }
            const date = new Date();
            const newLoad = new Load({
              created_by: userFromDB._id,
              assigned_to: null,
              name: body.name,
              payload: body.payload,
              pickup_address: body.pickup_address,
              delivery_address: body.delivery_address,
              dimensions: {
                width: body.dimensions.width,
                length: body.dimensions.length,
                height: body.dimensions.height,
              },
              created_date: date.toString(),
            });
            await newLoad.save();
            db.close();
            return res.status(200).json(
                {message: 'Load created successfully'});
          } catch (e) {
            db.close();
            return res.status(400).json(
                {message: 'Invalid request'});
          }
        }
      }
    });
  }
});

app.patch('/api/loads/active/state', checkToken,
    async function iterateLoadState(req, res) {
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'DRIVER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a DRIVER is authorized to access this url'});
              }
              const assignedLoad = await Load.findOne(
                  {assigned_to: userFromDB._id, status: 'ASSIGNED'});
              if (!assignedLoad) {
                db.close();
                return res.status(200).json(
                    {message: 'You do not have any active assigned Loads'});
              }
              if (assignedLoad.state === 'En route to Pick Up') {
                await Load.findOneAndUpdate(
                    {assigned_to: userFromDB._id, status: 'ASSIGNED'},
                    {state: 'Arrived to Pick Up'});
                db.close();
                return res.status(200).json({
                  message: 'Load state changed to Arrived to Pick Up'});
              } else if (assignedLoad.state === 'Arrived to Pick Up') {
                await Load.findOneAndUpdate(
                    {assigned_to: userFromDB._id, status: 'ASSIGNED'},
                    {state: 'En route to delivery'});
                db.close();
                return res.status(200).json({
                  message: 'Load state changed to En route to delivery'});
              } else if (assignedLoad.state === 'En route to delivery') {
                const load = await Load.findOneAndUpdate(
                    {assigned_to: userFromDB._id, status: 'ASSIGNED'},
                    {state: 'Arrived to delivery', status: 'SHIPPED'},
                    {new: true});
                const shippedDate = new Date();
                load.logs.push({message: 'Load is shipped',
                  time: shippedDate.toString()});
                load.save();
                await Truck.findOneAndUpdate(
                    {assigned_to: assignedLoad.assigned_to, status: 'OL'},
                    {status: 'IS', assigned_to: null}, {new: true});
                db.close();
                return res.status(200).json({
                  message: 'Load state changed to Arrived to delivery'});
              }
            } catch (e) {
              if (e) {
                db.close();
                return res.status(400).json(
                    {message: 'Invalid request'});
              }
            }
          }
        }
      });
    });

app.get('/api/loads/active', checkToken,
    async function getUserActiveLoad(req, res) {
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'DRIVER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a DRIVER is authorized to access this url'});
              }
              const isAnyAssignedLoad = await Load.findOne(
                  {assigned_to: userFromDB._id, status: 'ASSIGNED'});
              db.close();
              return res.status(200).json({
                load: {
                  _id: isAnyAssignedLoad._id,
                  created_by: isAnyAssignedLoad.created_by,
                  assigned_to: userFromDB._id,
                  status: isAnyAssignedLoad.status,
                  state: isAnyAssignedLoad.state,
                  name: isAnyAssignedLoad.name,
                  payload: isAnyAssignedLoad.payload,
                  pickup_address: isAnyAssignedLoad.pickup_address,
                  delivery_address: isAnyAssignedLoad.delivery_address,
                  dimensions: {
                    width: isAnyAssignedLoad.dimensions.width,
                    length: isAnyAssignedLoad.dimensions.length,
                    height: isAnyAssignedLoad.dimensions.height,
                  },
                  logs: isAnyAssignedLoad.logs,
                  created_date: isAnyAssignedLoad.created_date,
                },
              });
            } catch (e) {
              if (e) {
                db.close();
                return res.status(200).json({load: []});
              }
            }
          }
        }
      });
    });

app.get('/api/loads/:id', checkToken,
    async function getUserLoadById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role === 'SHIPPER') {
                const loadFromDB = await Load.findOne(
                    {created_by: userFromDB._id, _id: id});
                db.close();
                return res.status(200).json({
                  load: {
                    _id: id,
                    created_by: userFromDB._id,
                    assigned_to: loadFromDB.assigned_to,
                    status: loadFromDB.status,
                    state: loadFromDB.state,
                    name: loadFromDB.name,
                    payload: loadFromDB.payload,
                    pickup_address: loadFromDB.pickup_address,
                    delivery_address: loadFromDB.delivery_address,
                    dimensions: {
                      width: loadFromDB.dimensions.width,
                      length: loadFromDB.dimensions.length,
                      height: loadFromDB.dimensions.height,
                    },
                    logs: loadFromDB.logs,
                    created_date: loadFromDB.created_date,
                  },
                });
              } else if (userFromDB.role === 'DRIVER') {
                const loadFromDB = await Load.findOne(
                    {assigned_to: userFromDB._id, _id: id});
                db.close();
                return res.status(200).json({
                  load: {
                    _id: id,
                    created_by: userFromDB._id,
                    assigned_to: loadFromDB.assigned_to,
                    status: loadFromDB.status,
                    state: loadFromDB.state,
                    name: loadFromDB.name,
                    payload: loadFromDB.payload,
                    pickup_address: loadFromDB.pickup_address,
                    delivery_address: loadFromDB.delivery_address,
                    dimensions: {
                      width: loadFromDB.dimensions.width,
                      length: loadFromDB.dimensions.length,
                      height: loadFromDB.dimensions.height,
                    },
                    logs: loadFromDB.logs,
                    created_date: loadFromDB.created_date,
                  },
                });
              }
            } catch (e) {
              if (e) {
                db.close();
                return res.status(400).json(
                    {message: 'Load ID does not exist'});
              }
            }
          }
        }
      });
    });

app.put('/api/loads/:id', checkToken,
    async function updateUserLoadById(req, res) {
      const {body, params} = req;
      const updateLoadSchema = joi.object({
        name: joi.string().required(),
        payload: joi.number().required(),
        pickup_address: joi.string().required(),
        delivery_address: joi.string().required(),
        dimensions: joi.object().keys({
          width: joi.number().required(),
          length: joi.number().required(),
          height: joi.number().required(),
        }),
      });
      const {error} = updateLoadSchema.validate(body);
      const valid = error == null;
      if (!valid) {
        res.status(400).json({message: 'Type is incorrect',
        });
      } else {
        MongoClient.connect(process.env.DB_URI, async function(err, db) {
          if (err) {
            db.close();
            return res.status(500).json({message: 'Server error'});
          } else {
            const user = req.user;
            if (!user) {
              return res.status(400).json({message: 'Cannot find your token'});
            } else {
              try {
                const userFromDB = await User.findOne({email: user.email});
                if (userFromDB.role !== 'SHIPPER') {
                  db.close();
                  return res.status(400).json(
                      {message:
                      'Only a SHIPPER is authorized to access this url'});
                }
                const loadFromDB = await Load.findOne(
                    {created_by: userFromDB._id, _id: params.id});
                if (loadFromDB.status !== 'NEW') {
                  db.close();
                  return res.status(400).json(
                      {message: 'Only load with status NEW can be updated'});
                }
                await Load.findOneAndUpdate(
                    {created_by: userFromDB._id, _id: params.id},
                    {
                      name: body.name,
                      payload: body.payload,
                      pickup_address: body.pickup_address,
                      delivery_address: body.delivery_address,
                      dimensions: {
                        width: body.dimensions.width,
                        length: body.dimensions.length,
                        height: body.dimensions.height,
                      },
                    });
                db.close();
                return res.status(200).json(
                    {message: 'Load details changed successfully'});
              } catch (e) {
                if (e) {
                  db.close();
                  return res.status(400).json(
                      {message: 'Load ID does not exist or load is not new'});
                }
              }
            }
          }
        });
      }
    });

app.delete('/api/loads/:id', checkToken,
    async function deleteUserLoadById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            try {
              const userFromDB = await User.findOne({email: user.email});
              if (userFromDB.role !== 'SHIPPER') {
                db.close();
                return res.status(400).json(
                    {message:
                      'Only a SHIPPER is authorized to access this url'});
              }
              const loadFromDB = await Load.findOne(
                  {created_by: userFromDB._id, _id: id});
              if (loadFromDB.status !== 'NEW') {
                db.close();
                return res.status(400).json(
                    {message: 'Only load with status NEW can be deleted'});
              }
              await Load.findOneAndDelete(
                  {created_by: userFromDB._id, _id: id});
              db.close();
              return res.status(200).json(
                  {message: 'Load deleted successfully'});
            } catch (e) {
              if (e) {
                db.close();
                return res.status(400).json(
                    {message: 'Load ID does not exist or load is not new'});
              }
            }
          }
        }
      });
    });

// AUTH
app.post('/api/auth/register', async function createProfile(req, res) {
  const {body} = req;

  const profileSchema = joi.object({
    email: joi.string().required(),
    password: joi.string().required(),
    role: joi.string().valid('DRIVER', 'SHIPPER').required(),
  });

  const {error} = profileSchema.validate(body);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid request',
    });
  } else {
    const hash = bcrypt.hashSync(body.password, 10);

    MongoClient.connect(process.env.DB_URI, async function(err, db) {
      const ifExistUser = await User.findOne({email: body.email});
      if (err) {
        db.close();
        return res.status(500).json({message: 'Server error'});
      };
      if (ifExistUser) {
        db.close();
        return res.status(400).json({message: 'Username already exists'});
      }
      const date = new Date();
      const newUser = new User({
        role: body.role,
        email: body.email,
        hash: hash,
        created_date: date.toString(),
      });
      await newUser.save();
      db.close();
      return res.status(200).json({message: 'Profile created successfully'});
    });
  }
});

app.post('/api/auth/login', async function login(req, res) {
  const {body} = req;

  const profileSchema = joi.object({
    email: joi.string().required(),
    password: joi.string().required(),
  });

  const {error} = profileSchema.validate(body);
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid request',
    });
  } else {
    MongoClient.connect(process.env.DB_URI, async function(err, db) {
      if (err) {
        db.close();
        return res.status(500).json({message: 'Server error'});
      } else {
        const ifExistUser = await User.findOne({email: body.email});
        if (!ifExistUser) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        const result = bcrypt.compareSync(body.password, ifExistUser.hash);
        if (!result) {
          db.close();
          return res.status(400).json({message: 'Password is incorrect'});
        } else {
          const token = jwt.sign(
              {email: ifExistUser.email},
              process.env.TOKEN_KEY,
          );
          db.close();
          return res.status(200).json({
            jwt_token: `${token}`,
          });
        }
      }
    });
  }
});

app.listen(8080, () => {
  console.log('LISTENING ON PORT 8080');
});
