const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  created_date: {
    type: String,
    required: true,
  },
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = Truck;
